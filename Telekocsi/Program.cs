﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Telekocsi
{
    class Program
    {
        static List<Car> cars;
        static List<Claim> claims;

        static void Main(string[] args)
        {
            ElsoFeladat();

            MasodikFeladat();

            HarmadikFeladat();

            NegyedikFeladat();

            OtodikFeladat();

            HatodikFeladat();

            Console.WriteLine();
            Console.Write("Nyomjon meg egy gombot...");
            Console.ReadKey();
        }

        private static void HatodikFeladat()
        {
            var result = new List<string>();
            var takenCars = new List<Car>();

            foreach (var claim in claims)
            {
                var answer = $"{claim.Azonosító}:  Sajnos nem sikerült autót találni.";

                foreach (var car in cars)
                {
                    if (claim.Indulás == car.Indulás && claim.Cél == claim.Cél && claim.Személyek <= car.Férőhely && !takenCars.Contains(car))
                    {
                        takenCars.Add(car);

                        answer = $"{claim.Azonosító}: Rendszám: {car.Rendszám}, Telefonszám: {car.Telefonszám}";
                    }
                }
                result.Add(answer);
            }

            WriteResultsTotxt(result);

            Console.WriteLine($"Az eredmények fájlba írása megtörtént. Az eredményt megtalálja a {Environment.CurrentDirectory + @"\Output\"} címen.");
        }

        private static void WriteResultsTotxt(List<string> result)
        {
            if (!Directory.Exists(@"Output"))
            {
                Directory.CreateDirectory(@"Output");
            }

            using (var f = new StreamWriter(@"Output\utasuzenetek.txt", false, Encoding.UTF8))
            {
                foreach (var item in result)
                {
                    f.WriteLine(item);
                }
            }
        }

        private static void OtodikFeladat()
        {
            LoadClaimCSV();

            Console.WriteLine("5. feladat");
            foreach (var claim in claims)
            {
                foreach (var car in cars)
                {
                    if (claim.Indulás == car.Indulás && claim.Cél == claim.Cél && claim.Személyek <= car.Férőhely)
                    {
                        Console.WriteLine($"{claim.Azonosító} => {car.Rendszám}");
                        break;
                    }
                }
            }
            Console.WriteLine();
        }

        private static void LoadClaimCSV()
        {
            claims = new List<Claim>();

            using (var f = new StreamReader(@"CSV files\igenyek.csv"))
            {
                var headline = f.ReadLine();

                while (!f.EndOfStream)
                {
                    var row = f.ReadLine().Split(';');

                    var temporaryClaim = new Claim
                    {
                        Azonosító = row[0],
                        Indulás = row[1],
                        Cél = row[2],
                        Személyek = int.Parse(row[3])
                    };

                    claims.Add(temporaryClaim);
                }
            }
        }

        private static void NegyedikFeladat()
        {
            var result = new List<SeatOnTrip>();

            foreach (var item in cars)
            {
                var temporarySeatOnTrip = new SeatOnTrip
                {
                    Cél = item.Cél,
                    Indulás = item.Indulás,
                    Férőhely = item.Férőhely
                };

                var tmp = result.FirstOrDefault(r => r.Cél.Equals(temporarySeatOnTrip.Cél) && r.Indulás.Equals(temporarySeatOnTrip.Indulás));

                if (tmp != null)
                {
                    result.FirstOrDefault(r => r.Cél.Equals(temporarySeatOnTrip.Cél) && r.Indulás.Equals(temporarySeatOnTrip.Indulás)).Férőhely += temporarySeatOnTrip.Férőhely;
                }
                else
                {
                    result.Add(temporarySeatOnTrip);
                }
            }

            var maxSeat = result.Max(r => r.Férőhely);

            Console.WriteLine("4. feladat");
            Console.WriteLine($"A legtöbb férőhelyet ({maxSeat}-et/-at) a {result.FirstOrDefault(r => r.Férőhely.Equals(maxSeat)).Indulás}-{result.FirstOrDefault(r => r.Férőhely.Equals(maxSeat)).Cél} útvonalon ajánlottak fel a hírdetők.");
            Console.WriteLine();
        }

        private static void HarmadikFeladat()
        {
            Console.WriteLine("3. feladat");

            var result = 0;

            foreach (var item in cars)
            {
                if (item.Indulás == "Budapest" && item.Cél == "Miskolc")
                {
                    result += item.Férőhely;
                }
            }

            Console.WriteLine($"\t Összesen {result} férőhelyet hirdettek az autósok Budapestről Miskolcra.");
        }

        private static void MasodikFeladat()
        {
            Console.WriteLine("2. feladat");
            Console.WriteLine($"\t {cars.Count} autós hirdet fuvart.");
        }

        private static void ElsoFeladat()
        {
            cars = new List<Car>();

            using (var f = new StreamReader(@"CSV files\autok.csv", Encoding.Default))
            {
                var headline = f.ReadLine();

                while (!f.EndOfStream)
                {
                    var row = f.ReadLine().Split(';');

                    Car temporaryCar = new Car
                    {
                        Indulás = row[0],
                        Cél = row[1],
                        Rendszám = row[2],
                        Telefonszám = row[3],
                        Férőhely = int.Parse(row[4])
                    };

                    cars.Add(temporaryCar);
                }
            }
        }
    }
}
