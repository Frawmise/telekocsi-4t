﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Telekocsi
{
    public class Car
    {
        public Car()
        {

        }

        public string Indulás{ get; set; }
        public string Cél { get; set; }
        public string Rendszám { get; set; }
        public string Telefonszám { get; set; }
        public int Férőhely { get; set; }
    }
}
