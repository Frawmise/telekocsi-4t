﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Telekocsi
{
    public class SeatOnTrip
    {
        public string Indulás { get; set; }
        public string Cél { get; set; }
        public int Férőhely { get; set; }

        public SeatOnTrip()
        {

        }
    }
}
