﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Telekocsi
{
    public class Claim
    {
        public Claim()
        {

        }

        public string Azonosító { get; set; }
        public string Indulás { get; set; }
        public string Cél { get; set; }
        public int Személyek { get; set; }
    }
}
